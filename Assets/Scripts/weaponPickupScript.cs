﻿using UnityEngine;
using System.Collections;

public class weaponPickupScript : MonoBehaviour {

    public GameObject[] weapons;
    public GameObject swordHere;


    // Use this for initialization
    void Start() {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("swordPickup");
            Destroy(this.gameObject);
            
        }
    }
}
