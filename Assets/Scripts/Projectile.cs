﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float speed = 7;
    public float lifeTime = 2;
    public int damage;
    public LayerMask isHit;
    // Use this for initialization
    void Start()
    {
        if (GetComponent<Rigidbody2D>())
        {
            GetComponent<Rigidbody2D>().velocity =
                    new Vector2(speed, 0);
        }
       
        Destroy(gameObject, lifeTime);
    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnCollisionEnter2D(Collision2D projectile)
    {
        if (projectile.gameObject.tag != "Player" && projectile.gameObject.tag != "Ground")
            projectile.gameObject.SendMessage("hit",this.damage, SendMessageOptions.DontRequireReceiver);
         

        if (projectile.gameObject.tag != "Player")
            Destroy(gameObject);
    }
}
