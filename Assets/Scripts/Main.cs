﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour
{


    Rigidbody2D rb;
    public float runSpeed;
    public float jumpForce;

    public int MultiJump = 3;

    //this detects if im on ground
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public float groundCheckRadius;
    //handles animation
    public Animator anim;

    //handles flipping character
    public bool isFacingLeft;

    public int playerHealth;

    //health
    public GameObject[] healthBar;

    //Handles prefab instantiation(aka creation)
    public Transform projectileSpawnPoint;
    public Projectile projectile;
    public Projectile BigProjectile;
    public float ProjectileForce;

    bool swordPickedUp = false;
    bool isBig = false;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();

        if (!rb)
        {
            Debug.LogWarning("No Rigidbody2D found on GameObject");
        }
        //set run speed
        if (runSpeed == 0)
        {
            runSpeed = 1.5f;

            Debug.Log("runSpeed was not set. Defaulting to " + runSpeed);
        }
        //set jump height
        if (jumpForce == 0)
        {
            jumpForce = 1f;

            Debug.Log("jumpForce was not set. Defaulting to " + jumpForce);
        }
        //set groundCheck distance
        if (!groundCheck)
        {
            Debug.LogError("No groundCheck found on GameObject");
        }
        if (groundCheckRadius == 0)
        {
            groundCheckRadius = 0.095f;

            Debug.Log("groundCheckRadius was not set. Defaulting to " + groundCheckRadius);
        }

        //Animating charater
        anim = GetComponent<Animator>();
        if (!anim)
        {
            Debug.LogError("No Animator found on GameObject");
        }


    }

    void NormalBullet()
    {
        Projectile temp = Instantiate(projectile,
            projectileSpawnPoint.position,
            projectileSpawnPoint.rotation) as Projectile;

        anim.SetBool("ThumbUp", true);
        anim.CrossFade("Mario_ThumbUP", 0f);
        int side = 1;
        //if side is negative flip force
        if (isFacingLeft)
        {
            side = -1;
            Vector3 scaleFactor = temp.transform.localScale;
            scaleFactor.x *= -1; //scaleFactor.x = -scaleFactor.x; || same thing
            temp.transform.localScale = scaleFactor;

        }
        temp.GetComponent<Projectile>().speed = ProjectileForce * side;
    }

    void BigBullet()
    {
        isBig = false;
        anim.CrossFade("Kirby_Idle", 0f);
        Projectile temp = Instantiate(BigProjectile,
            projectileSpawnPoint.position,
            projectileSpawnPoint.rotation) as Projectile;

        anim.SetBool("ThumbUp", true);
        anim.CrossFade("Mario_ThumbUP", 0f);
        int side = 1;
        //if side is negative flip force
        if (isFacingLeft)
        {
            side = -1;
            Vector3 scaleFactor = temp.transform.localScale;
            scaleFactor.x *= -1; //scaleFactor.x = -scaleFactor.x; || same thing
            temp.transform.localScale = scaleFactor;

        }
        temp.GetComponent<Projectile>().speed = temp.GetComponent<Projectile>().speed * side;
    }

    // Update is called once per frame
    void Update()
    {

        if (groundCheck)
        {
            //checks if your grounded
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);
            Debug.Log("groundCheck found on GameObject");

            anim.SetBool("Grounded", isGrounded);
        }


        // Values returned are from -1 -> 1
        //move
        // Values returned are from -1,0,1
        float moveValue = Input.GetAxisRaw("Horizontal");



        //Single Jump
        //   if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        //  {
        //      rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        //   }
        if (isGrounded)
        {
            MultiJump = 3;
        }


        if (Input.GetKeyDown(KeyCode.Space) && (MultiJump >= 1))
        {
            MultiJump--;
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
            if(MultiJump <=1)
            {
                isBig = true;
            }
        }

        anim.SetFloat("MultiJump", Mathf.Abs(MultiJump));

        anim.SetBool("Jump", Input.GetKeyDown(KeyCode.Space));

        anim.SetBool("BigBullet",isBig);

        //run
        rb.velocity = new Vector2(moveValue * runSpeed, rb.velocity.y);
        //Animation set
        anim.SetFloat("MoveValue", Mathf.Abs(moveValue));


        anim.SetBool("ThumbUp", false);
        //Thumb up anim 
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if(!isBig)
            {
                if(swordPickedUp)
                {
                   NormalBullet();
                }

            }
            else
            {
                BigBullet();
            }
         


        }


        if (moveValue < 0 && !isFacingLeft)
            flip();
        else if (moveValue > 0 && isFacingLeft)
            flip();


    }

    void flip()
    {

        //isFacingLeft = !isFacingLeft; || Same as bottom if 
        if (isFacingLeft)
            isFacingLeft = false;
        else
            isFacingLeft = true;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1; //scaleFactor.x = -scaleFactor.x; || same thing

        transform.localScale = scaleFactor;

    }

    void hit(int damage)
    {
        playerHealth -= damage;
        updateHealthBar();

        if(playerHealth <=0)
        {
            GameManager.instance.gameOver();
        }
    }

    void addHealth(int health)
    {
        playerHealth += health;
        if(playerHealth > 3)
        {
            playerHealth = 3;
        }
        updateHealthBar();
    }

    void swordPickup()
    {
        swordPickedUp = true;
    }

    void updateHealthBar()
    {
        int ctr = 0;
        foreach(GameObject health in this.healthBar)
        {
            health.SetActive(false);
            if (ctr<this.playerHealth)
            {
                health.SetActive(true);
            }
            ctr++;
        }

    }
}
