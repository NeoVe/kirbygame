﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;//loads the game + other
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    //Creates a class variable to keep track of GameManger
    static GameManager _instance = null;

    //used to initiate player
    //public GameObject playerPrefab;

    //used to handle scoring system
    [SerializeField]
    int _score;
    public Text scoreText;

    //Pause variable
    public bool isPause;

    GameObject pauseScreen;

    // Use this for initialization
    void Start()
    {

        //check if GameManager instance already exists in Scene
        if (instance)
        {
            //GameManager exists,delete copy
            DestroyImmediate(gameObject);
        }
        else
        {
            //assign GameManager to variable "_instance"
            instance = this;

            //Do not destroy GameObject the script is attached too!
           // DontDestroyOnLoad(this);

        }


    }
    public void gameEnd()
    {
        SceneManager.LoadScene("GameOver");
    }
    // Update is called once per frame
   void Update()
     {
   /*     if (pauseScreen == null)
        {
            pauseScreen = GameObject.FindGameObjectWithTag("Pause");
            if (pauseScreen != null)
            {

                pauseScreen.SetActive(false);
            }
        }

    */
        //Click button to start next scene
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().name == "UI Scene")
            {
                SceneManager.LoadScene("Level_1_mario_week1");
            }

            else if (SceneManager.GetActiveScene().name == "Level_1_mario_week1")
            {
                SceneManager.LoadScene("UI Scene");
            }

            else
            {
                SceneManager.LoadScene("GameOver");
            }
            //assign 0 value to variable
            score = 0;



        }

        if (Input.GetKeyDown(KeyCode.P))
        {

            if (Time.timeScale == 0)
            {
                isPause = false;
                Time.timeScale = 1;

            }

            else
            {
                isPause = true;
                Time.timeScale = 0;


            }
            pauseScreen.SetActive(isPause);

        }


    }

    
    //Provides acces to private "_instance"
    //Variable must be declared
    //Variables must be static because method is static

    public static GameManager instance
    {
        get { return _instance; }//can also use just get;
        set { _instance = value; }//can also use just set;
    }
    //declare variable 'score' and create get/set function for it
    public int score
    {
        get
        {
            return _score;
        }

        set
        {
            _score = value;
            if (_score < 0)
                score = 0;

            if (scoreText)
                scoreText.text = "Score: " + _score.ToString("0000");
        }
    }

    public void StartGame()
    {
        //loads level1
        SceneManager.LoadScene("KirbyLevel1");
    }

    public void QuitGame()
    {

        //Quits game
        Debug.Log("Quit Game");
        Application.Quit();
        Debug.Break();
    }
    
    public void gameOver()
    {
        SceneManager.LoadScene("Death");
    }
}

