﻿using System.Collections;
using UnityEngine;

public class EnemyWalker : MonoBehaviour
{

    public float speed; //how fast enemy moves
    Rigidbody2D rb;        //How enemy moves
    public bool isFacingLeft;
    // Use this for initialization
    public int health;
    public int damage;
    void Start()
    {
     
        if (speed <= 0)
            speed = 1.0f;

        rb = GetComponent<Rigidbody2D>();
        if (!rb)
        {
            rb = gameObject.AddComponent<Rigidbody2D>();
        }

    }
    void flip()
    {

        //isFacingLeft = !isFacingLeft; || Same as bottom if 
        if (isFacingLeft)
            isFacingLeft = false;
        else
            isFacingLeft = true;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1; //scaleFactor.x = -scaleFactor.x; || same thing

        transform.localScale = scaleFactor;
        Debug.Log("Flip");

    }
    // Update is called once per frame
    void Update()
    {
        if (isFacingLeft)
            rb.velocity = new Vector2(-speed, rb.velocity.y);
        else
            rb.velocity = new Vector2(speed, rb.velocity.y);

    }
    private void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Player" || c.gameObject.tag == "Enemy" || c.gameObject.tag == "enemyHealth" || c.gameObject.tag == "AiWall")
        {

            flip();
        }

        if(c.gameObject.tag=="Player")
        {
            c.gameObject.SendMessage("hit", this.damage);
            GameManager.instance.score += -5;
        }
    }
    private void OnTriggerEnter2D(Collider2D c)
    {
        Debug.Log("GET HIT BRO!");
        if (c.gameObject.tag == "AiWall")
        {

            flip();
        }

    }

    public void hit(int damage)
    {

        health-=damage;

        // healthbar.sizeDelta = new Vector2(health * healthScale, healthbar.sizeDelta.y);
        //check if Enemy still has health
        if (health < 0)
        {
            GameManager.instance.score += 10;
            Destroy(gameObject);

        }
    }
}
