﻿using UnityEngine;
using System.Collections;

public class BreakMe : MonoBehaviour {

    public int score;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    private void OnTriggerEnter2D(Collider2D projectile)
    {
        if (projectile.gameObject.tag =="Player")
        {
             GameManager.instance.score += score;
             Destroy(gameObject);
        }
            
    }
   
}
