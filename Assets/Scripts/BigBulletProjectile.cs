﻿using UnityEngine;
using System.Collections;

public class BigBulletProjectile : Projectile {

	
	// Update is called once per frame
	void Update () {

        Vector3 position= transform.position;   
        position.x += speed;

        transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * Mathf.Abs(speed));
	}
    void OnTriggerEnter2D(Collider2D projectile)
    {
        Debug.Log("Test", projectile.gameObject);
        if (projectile.gameObject.tag != "Player" && projectile.gameObject.tag != "Ground")
            projectile.gameObject.SendMessage("hit",this.damage, SendMessageOptions.DontRequireReceiver);
    }
}
