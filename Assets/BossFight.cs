﻿using UnityEngine;
using System.Collections;

public class BossFight : MonoBehaviour {

    public int health;
    public int damage;
    public int jumpForce;

    public bool jump;
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public float groundCheckRadius;


    private Animator anim;
    private Rigidbody2D rb;
    private GameObject player;

    // Use this for initialization
    void Start ()
    {
        if (!groundCheck)
        {
            Debug.LogError("No groundCheck found on GameObject");
        }
        if (groundCheckRadius == 0)
        {
            groundCheckRadius = 0.095f;

            Debug.Log("groundCheckRadius was not set. Defaulting to " + groundCheckRadius);

        }
        this.rb = GetComponent<Rigidbody2D>();
        this.anim = GetComponent<Animator>();

        this.player = GameObject.FindGameObjectWithTag("Player");
      }
	
	// Update is called once per frame
	void Update () {
        if (groundCheck)
        {
            //checks if your grounded
            isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, isGroundLayer);
            Debug.Log("groundCheck found on GameObject");

            anim.SetBool("Grounded", isGrounded);
        }

        if(isGrounded)
        {
            this.jump = true;
        }
        if(this.jump)
        {
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag== "AiCeiling")
        {
            this.jump = false;
            Vector3 position = transform.position;
            position = new Vector3 (player.transform.position.x,position.y,position.z);
            transform.position = position;
        }
    }
}
