﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

    public int addHealth = 1;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            GameManager.instance.score += 5;
            other.gameObject.SendMessage("addHealth",this.addHealth);
            Destroy(this.gameObject);

        }
    }
}
